const path = require("path");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: "./_src/portfolio.js"
  },
  output: {
    path: path.resolve(__dirname, "_build"),
    filename: "app.bundle.js"
  },
    resolve: {
        alias: {
            d3: 'd3/build/d3.js'
        }
    },
    module: {
    rules: [
      {
        test: /\.pug/,
        use: 'pug-loader'
      },
      {
        test: /\.styl/,
        use: [
          "style-loader",
          "css-loader",
          "stylus-loader"
        ]
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        use: "file-loader"
      },
      {
        test: /\.js$|\.es6$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    open: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      // Change there title of your project (title in browser tab caption)
      title: 'KIT RU5 Portfolio',
      hash: true,
      // Change there name of main |pug| file
      template: './_src/index.pug'
    }),
    new UglifyJSPlugin()
  ]
};