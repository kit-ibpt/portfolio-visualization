"use strict;"
import Airtable from 'airtable'
var d3 = require('d3');
import Store from 'store'

export default class AirtableReader {

    constructor() {
        Airtable.configure(
            {
                endpointUrl: 'https://api.airtable.com',
                apiKey: 'key8mQhk706PiM6he'
            });


        this.base = Airtable.base('app4xPCRB1nBVTvj0');
        this.data = {};
        this.data.list = [];
        this.data.beamlines = [];

        this.readATPComponents()
    }


    readATPComponents() {
        var n = 0;

        this.base('ru5-infra-competence').select({
            maxRecords: 100,
            sort: [
                {field: 'order', direction: 'asc'}
            ],

        }).eachPage((records, fetchNextPage) => {

            this.data.list.push(
                ...records.map(function (record) {

                    const item = {};
                    item.id = record.id;
                    item.idx = n++;
                    item.collection = "atpComponents";
                    item.title = record.fields["title"];
                    item.name = record.fields["name"];
                    item.description = record.fields["description"];

                    return item
                })
            )
            fetchNextPage();

        }, (err) => { //done

            if (err) {
                console.error(err);
                return;
            }
            this.readBeamlines();

        });
    }


    readBeamlines() {
        var n = 0;

        this.base('beamlines').select({
            maxRecords: 100,
            sort: [
                {field: 'order', direction: 'asc'}
            ],

        }).eachPage((records, fetchNextPage) => {

            this.data.beamlines.push(
                ...records.map(function (record) {

                    const item = {};
                    item.id = record.id;
                    item.idx = n++;
                    item.collection = "beamlines";
                    item.title = record.fields["name"];
                    item.name = record.fields["name"];
                    item.method = record.fields["method"];
                    item.spectral_range = record.fields["spectral_range"];
                    item.order = record.fields["order"];

                    item.beamlineGroup = record.fields["method"] + " (" + record.fields["spectral_range"]
                    + ")"

                    return item
                })
            )
            fetchNextPage();


        }, (err) => { //done

            if (err) {
                console.error(err);
                return;
            }



            let beamlineGroups = d3.nest().key(d => d.method + "::" + d.spectral_range).entries(this.data.beamlines)

            beamlineGroups = beamlineGroups.map((d,i) =>{

                let split = d.key.split("::")

                return {
                    idx: i,
                    title: split[0],
                    name: split[0] + " (" + split[1] + ")",
                    description: split[1],
                    collection: "beamlineGroups",
                }}
            )


            //
            console.log(this.data.beamlines)
            //
            // beamlines.forEach((d,i) => d.idx = i)

            this.data.list.push(...beamlineGroups);
            this.data.list.push(...this.data.beamlines);



            this.readActivities();

        });
    }


    readActivities() {
        var n = 0;

        this.base('mt-activities').select({
            maxRecords: 100,
            sort: [
                {field: 'name', direction: 'asc'}
            ],

        }).eachPage((records, fetchNextPage) => {

            this.data.list.push(
                ...records.map(function (record) {

                    const item = {};
                    item.id = record.id;
                    item.idx = n++;
                    item.collection = "activities";
                    item.name = record.fields["name"];
                    item.topic = record.fields["topic"];

                    return item
                })
            )
            fetchNextPage();

        }, (err) => {  // done
            if (err) {
                console.error(err);
                return;
            }


            this.readTopics();

        });
    }

    readTopics() {
        var n = 0;

        this.base('mt-topics').select({
            maxRecords: 100,
            sort: [
                {field: 'name', direction: 'asc'}
            ],

        }).eachPage((records, fetchNextPage) => {

            this.data.list.push(
                ...records.map(function (record) {

                    const item = {};
                    item.id = record.id;
                    item.idx = n++;
                    item.collection = "mt-topics";
                    item.name = record.fields["name"];
                    item.title = record.fields["title"];
                    item.topic = record.fields["topic"];

                    return item
                })
            )
            fetchNextPage();

        }, (err) => {  // done
            if (err) {
                console.error(err);
                return;
            }


            this.readProjects();

        });
    }


    readProjects() {
        var n = 0;

        this.base('ru5-projects').select({
            maxRecords: 100,
            sort: [
                {field: 'order', direction: 'asc'}
            ],

        }).eachPage((records, fetchNextPage) => {

            this.data.list.push(
                ...records.map(function (record) {

                    const item = {};
                    item.collection = "projects";
                    item.idx = n++;
                    item.id = record.id;
                    item.name = record.fields["name"];
                    item.description = record.fields["description"];
                    item.startYear = +record.fields["startYear"];
                    item.endYear = record.fields["endYear"] || 2017;

                    item.years = [];
                    for (var year = item.startYear; year <= item.endYear; year++) {item.years.push(year)}
                    item.activities = record.fields["activities"];
                    item.topics = record.fields["mt-topics"];
                    item.atpComponents = record.fields["ru5-infra-comp"];

                    return item
                })
            )
            fetchNextPage();

        }, (err) => { // done
            if (err) {
                console.error(err);
                return;
            }


            this.data.map = this.data.list.reduce(function (map, obj) {
                map[obj.id] = obj;
                return map;
            }, {});

            this.data.nameIndex = this.data.list.reduce(function (map, obj) {
                map[obj.name] = obj.id;
                return map;
            }, {});

            this.data.nodes = this.data.list;

            this.data.links = this.data.list.reduce((links, obj) => {

                if (obj.collection === "beamlines") {

                    // links.push({"source": obj.id, "target": this.data.nameIndex["kara"], "type": "is-beamline"})

                }


                if (obj.atpComponents) {

                    obj.atpComponents.forEach(source => {

                        links.push({"source": source, "target": obj.id, "type": "used-atpComponents"})

                    })
                }

                if (obj.topics) {

                    obj.topics.forEach(source => {

                        links.push({"source": obj.id, "target": source, "type": "has-topic-" + this.data.map[source].topic})




                    })
                }

                

                return links;
            }, []);



            Store.set('data', this.data)


        });
    }
}