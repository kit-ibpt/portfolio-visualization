import css from "./index.styl"
import Store from 'store'

import AirtableReader from "./portfolioReader.js"
import Visualizer from "./portfolioVisualizer.js"

var observePlugin = require('store/plugins/observe')
Store.addPlugin(observePlugin)

const airtableReader = new AirtableReader()
let visualizer;
if (Store.get('data')) {visualizer = new Visualizer(Store.get('data'))}
Store.observe('data', (val, oldVal) => {

    console.log("observe")

    visualizer = new Visualizer(Store.get('data'))
})
