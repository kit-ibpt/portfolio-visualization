"use strict;"
var d3 = require('d3');
import {d3ForceAttract} from 'd3-force-attract'


export default class Visualizer {

    constructor(data) {

        const columns = {
            beamlineGroups: {
                collection: "beamlineGroups",
                span: 1,
                offset: 0,
                heightSpan: 9
            },
            beamlines: {
                collection: "beamlines",
                span: 1.2,
                offset: 0,
                heightSpan: 1
            },
            atpComponents: {
                collection: "atpComponents",
                span: 2.5,
                offset: 0,
                heightSpan: 9
            },
            projects: {
                collection: "projects",
                span: 2.5,
                offset: 2.5,
                heightSpan: 3
            },
            activities: {
                collection: "activities",
                span: 1,
                offset: 5.5,
                heightSpan: 1
            },
            "mt-topics": {
                collection: "mt-topics",
                span: 2,
                offset: 5,
                heightSpan: 3
            },
        }


        const collections = ["beamlineGroups", "atpComponents", "projects", "topics"];
        const lineHeight = 13.54;
        const columnGap = lineHeight * 2;

        var svg = d3.select('.svg-container').append('svg')
            .attr('width', width)
            .attr('height', height);


        function resize() {
            width = d3.select('div.svg-container').node().getBoundingClientRect().width;
            height = d3.select('div.svg-container').node().getBoundingClientRect().height;
            columnWidth = (width - (collections.length - 1) * columnGap) / collections.length;
            svg.attr("width", width).attr("height", height);
            simulation.force('center', d3.forceCenter(width / 2, height / 2));
            // simulation.force('attract', d3ForceAttract()
            //     .target([width / 2, height / 2])
            //     .strength(0.1))
        }

        var width = d3.select('div.svg-container').node().getBoundingClientRect().width;
        var height = d3.select('div.svg-container').node().getBoundingClientRect().height;
        var columnWidth = (width) / 6;


        svg.append("g")
            .classed("atp-box", true)
            .append("rect")
            .attr("x", columns["atpComponents"].offset * columnWidth - 0 * lineHeight)
            .attr("y", 3 * lineHeight)
            .attr("width", columns["atpComponents"].span * columnWidth - 3 * lineHeight)
            .attr("height", 700)

        var nodes = data.list.map(function (node) {

            return {
                id: node.id,
                ...node,
                fx: columnWidth * columns[node.collection].offset, //) + columnGap *
                fy: node.idx * columns[node.collection].heightSpan * lineHeight + lineHeight * 5,
                width: columns[node.collection].span * columnWidth - lineHeight * 3,
                height: columns[node.collection].heightSpan * lineHeight
            };
        });

        createGrid(svg);

        const beamlineGroupNames = nodes.filter(node => node.collection === "beamlineGroups").map(bg => bg.name)

        let heightSum = lineHeight * 5
        nodes.filter(node => node.collection === "projects")
            .forEach(
                (node, i) => {

                    let orderSplit = node.description.split("\n")
                    node.height = (orderSplit.length + 1) * lineHeight
                    node.fy = heightSum
                    heightSum += node.height
                }
            )



        nodes.filter(node => node.collection === "beamlines").forEach(
            (node, i) => {

                let orderSplit = node.order.split(".")

                node.fy =
                    beamlineGroupNames.indexOf(node.beamlineGroup) * columns["beamlineGroups"].heightSpan * lineHeight
                    + (orderSplit[1] - 1) * lineHeight * columns["beamlines"].heightSpan
                    + lineHeight * 1
                    + lineHeight * 5;

            }
        )


        nodes.filter(node => node.collection === "atpComponents").forEach(
            (node, i) => {
                if (i == 0) {
                    node.fy = 0
                } else if (i == 1) {
                    node.fy = 9 * lineHeight
                } else {

                    node.fy = 9 * lineHeight + (i) * 4.5 * lineHeight
                }

                node.fy += lineHeight * 5,
//                node.fy = node.idx + lineHeight * 20
                    node.height = (i < 3) ? 4.5 * lineHeight : 4 * lineHeight
            }
        )
        let lastTopic = null;
        let topicCount = 0;
        let lastSubtopic = null
        let subtopicCount = 0;

        nodes.filter(node => node.collection === "mt-topics")
            .forEach(
                (node, i) => {
                    let topic = node.name.split("-")[0]
                    let subtopic = node.name.split("-")[1]

                    if (topic != lastTopic) {
                        subtopicCount = 0
                        lastSubtopic = null
                        lastTopic = topic
                        topicCount++
                    }
                    if (subtopic != lastSubtopic) {

                        subtopicCount++
                        lastSubtopic = subtopic
                    }


                    node.fy = (topicCount - 1) * lineHeight * 6 +
                        (subtopicCount - 1) * lineHeight * 1 +
                        columns["mt-topics"].heightSpan * lineHeight * i + lineHeight * 20;

                }
            )


        var simulation = d3.forceSimulation()
            // keep entire simulation balanced around screen center
            //        .force('center', d3.forceCenter(width/2, height/2))

            // pull toward center
            //        .force('attract', d3.forceAttract()
            //            .target([width/2, height/2])
            //            .strength(0.0))
            //
            //        // cluster by section
            //        .force('cluster', d3.forceCluster()
            //            .centers(function (d) { return clusters[d.cluster]; })
            //            .strength(.0)
            //            .centerInertia(0.1))

            // apply collision with padding
            //        .force('collide', d3.forceCollide(function (d) { return d.radius + padding; })
            //            .strength(2))

            .force("link", d3.forceLink().id(function (d) { return d.id; }))
        //        .force("link", d3.forceLink(strategy.links).id(d => d.id).distance(20).strength(1))
        simulation
            .nodes(nodes)
            .on("tick", layoutTick);
        simulation.force("link")
            .links(data.links);


        var node = svg.selectAll('.node')
            .data(nodes)
            .enter()
            .append('g')
            .classed("node--project", d => d.collection === "projects")
            .classed("node--activities", d => d.collection === "activities")
            .classed("node--atpComponents", d => d.collection === "atpComponents")
            .classed("node--beamlines", d => d.collection === "beamlineGroups")


        var link = svg.selectAll("path")
            .data(data.links)
            .enter()
            .append("path")
            .attr("class", function (d) { return "link " + d.type; })
            .attr("marker-end", function (d) { return "url(#" + d.type + ")"; });

        //
        // node.filter(d => {return d.collection === "beamlines"})
        //     .append("line")
        //     .classed("horizontal-small", true)
        //     .classed("beamline", true)
        //     .attr("x1", d => 0)
        //     .attr("x2", d => {return d.width})
        //     .attr("y1", -0 * lineHeight)
        //     .attr("y2", -0 * lineHeight)
        //
        // node.filter(d => {return d.collection === "beamlineGroups"})
        //     .append('rect')
        //     .classed("beamline__title-bg", true)
        //     .attr("id", (d, i) => "beamline__title-bg-" + i)
        //     .attr("height", lineHeight * .5)
        //     .attr("width", lineHeight * 0)
        //     .attr("y", -lineHeight * .5)
        //
        //
        // node.filter(d => {return d.collection === "beamlineGroups"})
        //     .append('text')
        //     .classed("beamlineGroup__range", true)
        //     // .attr("dx", lineHeight)
        //     .text(d => d.description)
        //     .attr("dx", lineHeight / 4)
        //     .attr("dy", -lineHeight / 4 * 3)
        //     .each(function (e, i) {
        //         let w = this.getBoundingClientRect().width + lineHeight / 4;
        //         svg.select("#beamline__title-bg-" + i).attr("width", w)
        //     })
        //
        // node.filter(d => {return d.collection === "beamlineGroups"})
        //     .append('text')
        //     .classed("beamlineGroup__title", true)
        //     // .attr("dx", lineHeight)
        //     .text(d => d.title)
        //     .attr("dx", lineHeight / 4)
        //     .attr("dy", 0 * lineHeight / 4 * 3)
        //     .each(function (e, i) {
        //         let w = this.getBoundingClientRect().width + lineHeight / 4;
        //         svg.select("#beamline__title-bg-" + i).attr("width", w)
        //     })
        //
        //
        // node.filter(d => {return d.collection === "beamlines"})
        //     .append('text')
        //     .classed("beamline__title", true)
        //     // .attr("dx", lineHeight)
        //     .text(d => d.title)
        //     .attr("dx", lineHeight / 4)
        //     .each(function (e, i) {
        //         let w = this.getBoundingClientRect().width + lineHeight / 4;
        //         svg.select("#beamline__title-bg-" + i).attr("width", w)
        //     })


        node.filter(d => {return d.collection === "atpComponents"})
            .append("line")
            .classed("horizontal-small", true)
            .attr("x1", d => 0)
            .attr("x2", d => {return d.width})
            .attr("y1", 0 * lineHeight)
            .attr("y2", 0 * lineHeight)

        node.filter(d => {return d.collection === "atpComponents"})
            .append('rect')
            .classed("atpComponent__title-bg", true)
            .attr("id", (d, i) => "atpComponent__title-bg-" + i)
            .attr("height", lineHeight * .5)
            .attr("width", lineHeight * 5)
            .attr("y", -lineHeight * .5)
            .attr("x", lineHeight * .5)

        node.filter(d => {return d.collection === "atpComponents"})
            .append('text')
            .classed("atpComponent__title", true)

            .text(d => d.title)
            .attr("dx", lineHeight * .75)

            .each(function (e, i) {
                let w = this.getBoundingClientRect().width + lineHeight / 2;
                svg.select("#atpComponent__title-bg-" + i).attr("width", w)
            })


        node.filter(d => {return d.collection === "atpComponents"})
            .append('text')

            .classed("atpComponent__description", true)
            .selectAll("tspan")
            .data(d => d.description.match(/[^\r\n]+/g))
            .enter()
            .append("tspan")
            .attr("dy", lineHeight)
            .attr("x", lineHeight * .75)
            .text(d => d)


        node.filter(d => {return d.collection === "projects"})
            .append("line")
            .classed("horizontal-small", true)
            .attr("x1", d => 0)
            .attr("x2", d => {return d.width})
            .attr("y1", -0 * lineHeight)
            .attr("y2", -0 * lineHeight)

        let years = node.filter(d => {return d.collection === "projects"})
            .append('g')
            .classed("years", true)
            .selectAll("text")
            .data(d => {return d.years})
            .enter();

        years
            .append("rect")
            .attr('class', function (d) { return "project__year-bg"; })
            .attr('y', function (d) { return -lineHeight * .5; })
            .attr('x', function (d) { return 2 * columnWidth - 1.5 * lineHeight + lineHeight + (d - 2017) * lineHeight * .5; })
            .attr('width', function (d) { return lineHeight * .5; })
            .attr('height', function (d) { return lineHeight * .5; })


        years
            .append("text")
            .text(d => "" + (d - 2000))
            .attr('class', function (d) { return "project__year"; })
            .attr('y', function (d) { return -lineHeight * .0; })
            .attr('x', function (d) { return 2 * columnWidth - 1 * lineHeight + lineHeight + (d - 2017) * lineHeight * 1; })


        node.filter(d => {return d.collection === "projects"})
            .append('rect')
            .classed("project__title-bg", true)
            .attr("id", (d, i) => "project__title-bg-" + i)
            .attr("height", lineHeight * .5)
            .attr("width", lineHeight * 5)
            .attr("y", -lineHeight * .5)
            .attr("x", lineHeight * .5)

        node.filter(d => {return d.collection === "projects"}).append('text')
            .classed("project__title", true)
            .attr("dx", lineHeight * .75)
            .text(d => d.name)
            .each(function (e, i) {
                let w = this.getBoundingClientRect().width + lineHeight / 2;
                svg.select("#project__title-bg-" + i).attr("width", w)
            })


        node.filter(d => {return d.collection === "projects"})
            .append('text')
            .classed("project__description", true)
            .attr("dy", lineHeight)
            .selectAll("tspan")
            .data(d => d.description.match(/[^\r\n]+/g))
            .enter()
            .append("tspan")
            .attr("dy", lineHeight)
            .attr("x", lineHeight * .75)
            .text(d => d)


        node.filter(d => {return d.collection === "mt-topics"})
            .append("line")
            .classed("horizontal-small", true)
            .classed("activities-ard", d => d.name.startsWith("ARD"))
            .classed("activities-dts", d => d.name.startsWith("DTS"))
            .attr("x1", 0)
            .attr("x2", d => {return d.width})
            .attr("y1", 0 * lineHeight)
            .attr("y2", 0 * lineHeight)


        let actParts = node.filter(d => {return d.collection === "mt-topics"})
            .append('g')
            .classed("parts", true)
            .classed("collection--activities-ard", d => d.name.startsWith("ARD"))
            .classed("collection--activities-dts", d => d.name.startsWith("DTS"))
            .append('text')
            .append("tspan")
            .attr("x", lineHeight * .75)
            .text(d => {return d.name})
            .selectAll("tspan")
            .data(d => d.title.match(/[^\r\n]+/g))
            .enter()
            .append("tspan")
            .attr("dy", lineHeight)
            .attr("x", lineHeight * .75)
            .text(d => d)
            //
            // .append("tspan")
            // .attr("x", 0)
            // .attr("dy", lineHeight)
            // .text(d => {return d.title})
            // .enter()

        // node.filter(d => {return d.collection === "activities"})
        //     .append("line")
        //     .classed("horizontal-small", true)
        //     .classed("activities-ard", d => d.name.startsWith("ARD"))
        //     .classed("activities-dts", d => d.name.startsWith("DTS"))
        //     .attr("x1", 0)
        //     .attr("x2", d => {return d.width})
        //     .attr("y1", 0 * lineHeight)
        //     .attr("y2", 0 * lineHeight)
        //
        //
        // let actParts = node.filter(d => {return d.collection === "activities"})
        //     .append('g')
        //     .classed("parts", true)
        //     .classed("collection--activities-ard", d => d.name.startsWith("ARD"))
        //     .classed("collection--activities-dts", d => d.name.startsWith("DTS"))
        //     .append('text')
        //     .selectAll("tspan")
        //     .data(d => {return d.name.split("-")})
        //     .enter()

        // actParts
        //     .append('tspan')
        //     .classed("label", true)
        //     .text(d => d + ".")


        //
        //
        // function dragstarted(d) {
        //     if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        //     d.fx = d.x;
        //     d.fy = d.y;
        // }
        //
        // function dragged(d) {
        //     d.fx = d3.event.x;
        //     d.fy = d3.event.y;
        // }
        //
        // function dragended(d) {
        //     if (!d3.event.active) simulation.alphaTarget(0);
        //     d.fx = null;
        //     d.fy = null;
        // }

        // ramp up collision strength to provide smooth transition
        var transitionTime = 100;
        var t = d3.timer(function (elapsed) {
            var dt = elapsed / transitionTime;
//        simulation.force('collide').strength(Math.pow(dt, 2) * 0.7);
            if (dt >= 1.0) t.stop();
        });

        function layoutTick(e) {
            node
                .attr('transform', function (d) { return "translate(" + d.x + ", " + d.y + ")"; })

            link.attr("d", linkArc);


//        link
////            .each(function(d) { d.source.y -= k, d.target.y += k; })
//            .attr("x1", function(d) { return d.source.x; })
//            .attr("y1", function(d) { return d.source.y; })
//            .attr("x2", function(d) { return d.target.x; })
//            .attr("y2", function(d) { return d.target.y; });


        }

        var lineGenerator = d3.line()
            .curve(d3.curveCardinal);

        function linkArc(d) {
//        var dx = d.target.x - d.source.x,
//            dy = d.target.y - d.source.y,
//            dr = Math.sqrt(dx * dx + dy * dy);

//        var pathData = lineGenerator([[d.source.x, d.source.y], [d.target.x, d.target.y]]);

//        return "M" + d.source.y + "," + d.source.x
//            + "C" + (d.source.y + d.target.y) / 2 + "," + d.source.x
//            + " " + (d.source.y + d.target.y) / 2 + "," + d.target.x
//            + " " + d.target.y + "," + d.target.x;


            let sourceX = d.source.x + d.source.width;
            let sourceY = d.source.y - lineHeight * 0

            let targetX = d.target.x - lineHeight * 0; // + d.target.width;
            let targetY = d.target.y - lineHeight * 0


            return "M" + sourceX + "," + sourceY
                               // + "C" + (sourceX + targetX) / 2 + "," + sourceY
                               // + " " + (sourceX + targetX) / 2 + "," + targetY
                + " " + targetX + "," + targetY;
//
//        return d3.linkVertical()
//            .x(function(d) { return d.source.x; })
//            .y(function(d) { return d.x; })(d);
////        return pathData;
//        return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
        }


        resize();
        d3.select(window).on("resize", resize);


        function createGrid(svg) {

            svg.selectAll("line.grid").data(d3.range(70)).enter()
                .append("line")
                .attr("class", "grid")
                .attr("x1", 0)
                .attr("x2", width)
                .attr("y1", d => d * lineHeight)
                .attr("y2", d => d * lineHeight)


        }


    }
}
